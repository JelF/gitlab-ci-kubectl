#!/bin/sh

set -ex

apt-get update
apt-get install -y apt-transport-https curl gnupg

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | \
  apt-key add -

echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >> \
  /etc/apt/sources.list.d/kubernetes.list

apt-get update
apt-get install -y kubectl
apt-get clean
