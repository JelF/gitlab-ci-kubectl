#!/bin/sh

set -ex

if test ! $KUBE_TOKEN; then
  test -f $KUBE_TOKEN_LOCATION && export KUBE_TOKEN=$(cat $KUBE_TOKEN_LOCATION)
fi

kubectl config set-cluster default --server="$KUBESERVER"
kubectl config set-credentials default --token="$KUBE_TOKEN"
kubectl config set-context --current --cluster=default --user=default

$@
