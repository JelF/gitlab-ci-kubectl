FROM ubuntu:xenial
COPY scripts/install.sh scripts/install.sh
RUN ["sh", "/scripts/install.sh"]

COPY scripts/entrypoint.sh /scripts/entrypoint.sh
ENTRYPOINT ["sh", "/scripts/entrypoint.sh"]
CMD ["kubectl"]
ENV KUBE_TOKEN_LOCATION=/var/run/secrets/kubernetes.io/serviceaccount/token \
  KUBE_SERVER=https://kubernetes.default.svc \
  KUBE_TOKEN=''
