# gitlab-ci-kubectl

`kubectl` image for gitlab-ci.

## Variables

|Variable|Meaning|Default value|
|---|---|---|
|KUBE_SERVER|Kubernetes apiserver URL|https://kubernetes.default.svc|
|KUBE_TOKEN|Kubernetes service account token|`$(cat $KUBE_TOKEN_LOCATION)`|
|KUBE_TOKEN_LOCATION|Location of service account token|/var/run/secrets/kubernetes.io/serviceaccount/token|

## Usage example

```yaml
deploy:
  image: registry.gitlab.com/jelf/gitlab-ci-kubectl:latest
  stage: deploy
  when: manual
  variables:
    IMAGE: *image
    PATCH: |
      {
        "spec": {
          "template":
            { "spec":
              { "containers": [
                {
                  "name": "nginx",
                  "image": "$IMAGE:$CI_COMMIT_SHA"
                }
              ]
            }
          }
        }
      }
  tags:
    - kube-production
  script:
    - kubectl patch deployment nginx -p "$PATCH"
```
